// Compiled ECMAScript 6

/* firebase */
"use strict";

var firebase;
var myName;
var roomList;
var message;
var messageMng;

window.addEventListener('load', init(), false);

// 初期処理
function init() {
  // アプリケーション用のデータストアの参照を取得
  var config = {
    apiKey: "AIzaSyC6_Z-yAshCWssf9lvq7UIhuu9AKpb7fGE",
    authDomain: "teammogera.firebaseapp.com",
    databaseURL: "https://teammogera.firebaseio.com",
    storageBucket: "teammogera.appspot.com",
    messagingSenderId: "269047252464"
  };
  firebase.initializeApp(config);

  // anonymous sign in.
  firebase.auth().signInAnonymously()["catch"](function (error) {
    // Handle Errors here.
    console.log("firebase_auth_error");
    var errorCode = error.code;
    var errorMessage = error.message;
  });

  getRoomList();
};

// ルームリスト取得
function getRoomList() {
  roomList = [];
  firebase.database().ref().on('child_added', function (dataSnapshot) {
    roomList.push(dataSnapshot.key);
  });
}

// ルーム新規作成
function createRoom() {
  var roomName = prompt('ルーム名は？');
  if (roomName && roomName != '') joinRoom(roomName);
}

// ルームに入る
function joinRoom(roomName) {
  message = '';

  messageMng = firebase.database().ref().child(roomName + '/messages');
  messageMng.push({ 'message': myName + 'が入室しました' });

  messageMng.on('child_added', function (dataSnapshot) {
    var data = dataSnapshot.val();
    message += (data.name ? data.name + ' : ' : '') + data.message + '\n';
    riot.update();
  });
}

// ルームを離れる
function leaveRoom() {
  messageMng.push({ 'message': myName + 'が退室しました' });
  messageMng.off('child_added');
  messageMng = undefined;
}

// メッセージ送信
function sendMsg(msg) {
  messageMng.push({
    'name': myName,
    'message': msg
  });
}